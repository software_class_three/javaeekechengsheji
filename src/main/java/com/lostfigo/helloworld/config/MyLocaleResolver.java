package com.lostfigo.helloworld.config;

import com.sun.corba.se.spi.orbutil.closure.Closure;
import com.sun.corba.se.spi.resolver.LocalResolver;
import org.omg.CORBA.Object;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Set;

/**
 * @program: helloworld
 * @description:
 * @author: Lost_Figo
 * @create: 2020-05-12 08:27
 **/
public class MyLocaleResolver implements LocaleResolver {

    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {

        // 获取请求中的语言参数
        String language = httpServletRequest.getParameter("l");

        Locale locale = Locale.getDefault();

        // 如果请求的链接携带了国际化的参数
        if(!StringUtils.isEmpty(language)){
            String[] split = language.split("_");

            // 国家地区
            locale = new Locale(split[0], split[1]);
        }

        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {

    }
}
