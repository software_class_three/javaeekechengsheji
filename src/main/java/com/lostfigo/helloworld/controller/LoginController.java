package com.lostfigo.helloworld.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * @program: helloworld
 * @description:
 * @author: Lost_Figo
 * @create: 2020-05-12 08:44
 **/
@Controller
public class LoginController {

    @RequestMapping("/user/login")
    public String login(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            Model model,
            HttpSession session){

        if(!StringUtils.isEmpty(username) && "123456".equals(password)){
            session.setAttribute("loginUser",username);
            return "redirect:/main.html";
        }else {
            // 告诉用户，登录失败
            model.addAttribute("msg","用户名或密码错误!");
            return "index";
        }

    }

    @RequestMapping("/user/logout")
    public String loginout(HttpSession session){
        session.invalidate();
        return "redirect:/index.html";
    }
}
