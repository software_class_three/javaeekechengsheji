package com.lostfigo.helloworld.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: helloworld
 * @description:
 * @author: Lost_Figo
 * @create: 2020-05-11 19:22
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department {
    private Integer id;
    private String departmentName;
}
